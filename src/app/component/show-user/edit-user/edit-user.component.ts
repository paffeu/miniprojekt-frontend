import {Component, Input, OnInit} from '@angular/core';
import {UserModel} from '../../../shared/model/user.model';
import {UserService} from '../../../shared/service/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  @Input()
  user: UserModel = new UserModel();
  sub: any;

  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => this.user.id = params['id']);
    this.userService.getUser(this.user.id).subscribe((response) => this.user = response,
      (error) => console.log(error));
  }

  editUser() : void {
    console.log(this.user);
    this.userService.editUser(this.user).subscribe((response) => console.log(response),
      (error) => console.log(error));
  }

  /*ngOnDestroy() {
    this.sub.unsubscribe();
  }*/


}
