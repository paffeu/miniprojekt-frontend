import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditUserComponent} from './edit-user/edit-user.component';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {RouterModule} from '@angular/router';
import {ShowUserComponent} from './show-user.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    ButtonModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    ShowUserComponent,
    EditUserComponent
  ],
  declarations: [
    ShowUserComponent,
    EditUserComponent
  ]
})
export class ShowUserModule { }
