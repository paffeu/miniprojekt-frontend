import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../shared/model/user.model';
import {UserService} from '../../shared/service/user.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {

  userList: UserModel[];
  selectedUser: UserModel;
  navigationSubscription;

  cols = [
    { field: 'firstName', header: 'Imię' },
    { field: 'lastName', header: 'Nazwisko' },
    { field: 'nickName', header: 'Ksywa' },
    { field: 'email', header: 'E-mail' }
  ];

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.onReload();
      }
    });
  }

  ngOnInit() {
    this.getAllUsers();
    setTimeout(() => this.getAllUsers(), 200);
  }

  getAllUsers(): void {
    this.userService.getAllUsers().subscribe((response) => this.userList = response,
      (error) => console.error(error));
  }

  onSelectRow()
  {
    this.router.navigate(['../edit-user', this.selectedUser.id], { relativeTo: this.route });
  }

  onDeleteButtonClick(user: UserModel)
  {
    this.userService.deleteUser(user.id).subscribe((response) => console.log(response),
      (error) => console.log(error));
    this.router.navigate(['../show-user'], { relativeTo: this.route });
  }

  onReload()
  {
    setTimeout(() => this.getAllUsers(), 200);
  }

}
