import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CreateUserComponent} from './create-user.component';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    RouterModule
  ],
  exports: [
    CreateUserComponent
  ],
  declarations: [
    CreateUserComponent
  ]
})
export class CreateUserModule { }
