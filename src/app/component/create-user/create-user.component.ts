import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../shared/model/user.model';
import {UserService} from '../../shared/service/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  user: UserModel = new UserModel();
  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onCreateButtonClick()
  {
    this.createUser();
    this.router.navigate(['../show-user'], { relativeTo: this.route });
  }

  createUser() : void {
    console.log(this.user);
    this.user.setUUID();
    this.userService.createUser(this.user).subscribe((response) => {
      console.log(response)
    }, (error) => {
      console.log(error);
    })

  }





  /*logOnConsole(message: String) {
    console.log(message);
  }*/

}
