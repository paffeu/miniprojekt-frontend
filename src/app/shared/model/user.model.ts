import * as uuid from 'uuid';

export class UserModel {

  id: 0;
  firstName: string;
  lastName: string;
  email: string;
  nickName: string;
  cardId: string;

  constructor() {
    this.id = null;
    this.firstName = null;
    this.lastName = null;
    this.nickName = null;
    this.email = null;
  }

  setUUID()
  {
    this.cardId = uuid();
  }
}
