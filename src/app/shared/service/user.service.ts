import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {UserModel} from '../model/user.model';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  //CREATE
  createUser(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(
      `${environment.url}/user/add`, user
    );
  }

  //READ
  getAllUsers(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(
      `${environment.url}/user/all`
    );
  }

  getUser(id: number): Observable<UserModel> {
    return this.http.get<UserModel>(
      `${environment.url}/user/${id}`
    )
  }

  //UPDATE
  editUser(user: UserModel): Observable<UserModel> {
    return this.http.put<UserModel>(
      `${environment.url}/user/${user.id}/edit`, user
    );
  }

  //DELETE
  deleteUser(id: number): Observable<UserModel> {
    return this.http.delete<UserModel>(
      `${environment.url}/user/${id}/delete`
    )
  }
}
