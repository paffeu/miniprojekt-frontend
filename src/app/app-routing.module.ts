import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {CreateUserComponent} from './component/create-user/create-user.component';
import {MenuComponent} from './component/menu/menu.component';
import {ShowUserComponent} from './component/show-user/show-user.component';
import {EditUserComponent} from './component/show-user/edit-user/edit-user.component';


const appRoutes: Routes = [
  { path: '', component: MenuComponent },
  { path: 'create-user', component: CreateUserComponent },
  { path: 'show-user', component: ShowUserComponent,
    runGuardsAndResolvers: 'always'},
  { path: 'edit-user/:id', component: EditUserComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
